
## About The Project

Project helps easy to deploy a new calServer System on an Docker environment, by using basic compose files.  


### Installation


#### 1. start creating an docker environment

further calServer needs docker and docker-compose for running

##### reset existing docker and docker-compose

```
remove-all-from-docker.sh
```

##### reset existing docker and docker-compose

```
install-docker.sh
```

#### 2. go to the calserver home directory or create

```
mkdir /var/calserver
cd /var/calserver
```

#### 3. Clone the repo

as required feature GIT needs installed on the server. If not installed you need to do before.
Than clone the startup repo.

 ```
 git clone https://calhelp@bitbucket.org/calserver/calserver-start.git
 ```

#### 4. go to the installer path and copy the templates


 ```
 cd calserver-start
 ./install.sh
 ```

 * will copy all yml and sh from src to directory below
 * will copy .env from template
 * will add /var/calserver/deploy as service for autostart(change path in .service file if home directory is different)
 * will make all shell scripts run enabled

or update  

 ```
 cd calserver-start
 ./copy_to_master.sh
 ```

* will copy all yml and sh from src to directory below
* will make all shell scripts run enabled


#### 5. edit the environment data

go back to master directory and change the environment variables as your needs or is that update pls check environment file
for further informations about environment variables take a look to [calserver.md](calserver.md) in this repo

 ```
 cd ..
 nano .env
 ```


#### 6. update environment

load your version of your ordered docker container

 ```
 ./update.sh
 ```


ℹ️ INFO:
Keep in mind: Docker hub login is required to successful transfer containers
docker login -u'Your Username' -p
type your password if asked



#### 7. deploy calserver

start your version of your ordered docker container

 ```
 ./deploy.sh
 ```


### Transfer data from different system

 for environment changes it is required to transfer files and database to an different server. this can don with


#### copy data files
if only copy once is needed

```
scp -r root@YourOldServer:/var/filemanager/ /data/calserver/filemanager
```
 Keep in mind: first part of command is source and second is the target, pathes in command below are samples. change to your needs.


#### synchronize data files
if sync is your favorite (recommandation)

```
rsync -avz -e 'ssh' root@YourOldServer:/var/filemanager/backup/
/data/calserver/filemanager
```

Keep in mind: first part of command is source and second is the target, pathes in command below are samples. change to your needs.

##### in two steps:
export at source server in sql file:

```
mysqldump -u root -p --opt [database name] > [database name].sql
```

import at target server from copied sql file:

```
# mysql -u root -p newdatabase < /path/to/newdatabase.sql
```

###### all in one command:

```
# mysqldump -h YourSourceIP -uYourDBUserSource -pYourDBPassSource metbase | mysql -h localhost -uYorDBUserTarget -pYourDBPassTarget metbase
```
