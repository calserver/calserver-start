# calServer

 is a cloud-enabled, mobile-ready, offline-storage, lab management software to handle assets and ther options multi tenancy customer based. It includes:

  - multi-dashboard for individual widget using
  - inventory management with types, prices status and filter operations
  - calibration, file, maintenance, task, history options for inventories
  - customer management with advanced roles and group management
  - advanced order management with offer, order, delivery and billing functions
  - ticket management with risk analysis
  - automatic email, backup status action handling
  - .... and many many more ...


# New Features!

  - statistic module to filter calibrations and give countings in dashboard
  - automatic 3 base risk management for tickets

You can also:
  - use procedure editor to manage calibration result runtime
  - export each filtered grid or send it directly to users

# Contents


[TOC]

### Requirements

   - A 64-bit host with at least 2GB RAM and 2 cores.
     - 1 core hosts would work but not optimally.
   - Docker (for Docker versions of code-server).

### Tech

calServer uses a number of open source projects to work properly:

* [YII2](http://yii-framework.com)  - HTML enhanced for web apps!
* [AdminLTE] - awesome web-based text editor
* [Bootstrap 3] - Markdown parser done right. Fast and easy to extend.

And of course calServer itself is open source with licensed ....

### Installation

calServer requires [docker.io](https://docker.io)  to run.

Install prequeresites:

```sh
$ apt install docker
$ apt install docker-compose
$ mkdir /var/calserver
$ cd /var/calserver
```

login to your docker hub account via ssh commandline:
```sh
$ docker login -u UserName -p
```
if login successfull, tha calServer license admin has enabled your account with active subscription for UserName

Install with docker-compose with the minimum of docker-compose.yml file like:

```yml
version: '3.6'

services:
  calserver:
    image: calhelp/calwebapp_bootstrap:${IMAGE_TAG}
    container_name: calserver
    restart: always
    environment:
      - DOMAIN=${DOMAIN}
      - DB_HOST=${DB_HOST}
      - DB_DATABASE=${DB_DATABASE}
      - DB_USER=${DB_USER}
      - DB_PASSWORD=${DB_PASSWORD}
    volumes:
      - ${FILE_MANAGER_PATH}:/app/httpdocs/filemanager
      - ${LOCAL_PATH}:/var/local
    ports:
      - 80:80
```

and create  with docker-compose with docker-compose.yml file like:

```yml
IMAGE_TAG=develop-latest

DOMAIN=localhost
LETSENCRYPT=false

# MySQL Database
DB_HOST=mysql_db
DB_ROOT_PASSWORD=calserver01
DB_DATABASE=calserver_develop
DB_USER=root
DB_PASSWORD=myPassword

# Configure paths
FILE_MANAGER_PATH=/data/calserver/filemanager/data
LOCAL_PATH=/data/calserver/local
```

usable variables(if using whole package with proxy) are:

| variable | description | standard | values |
| ------ | ------ | ------ | ------ |
| **Source Settings** |  |  |  |
| IMAGE_TAG | repo from docker hub | release-latest | release-latest, develop-latest |
| DOMAIN | name of the domain or localhost | your.domain.name |
| SUBSCRIPTION | your start subscription | EXPERT | FREE, BASIS, PROFESSIONAL, EXPERT, ALL |
| LETSENCRYPT | enable letsencrypt bot | true | true, false |
| LETSENCRYPT_EMAIL| email adress used for letsencrypt bot | | |
| DOCKER_PATH | path for docker shell scripts | /var/calserver | |
| SSL_CERT_PATH | path where user defined SSL files located | /var/calserver/ssl | |
| **MySQL Database** | | | |
| DB_HOST | mysql host server | mysql_db | |
| DB_ROOT_PASSWORD | root password of mysql | calserver01 ||
| DB_DATABASE | | calserver_develop ||
| DB_USER | db user for calServer database | root | |
| DB_PASSWORD | password for calServer database | calserver01 | |
| MYSQL_DATA_PATH | mysql data path | /data/mysql | |
| **SyBase Database** | if customer has also metbase server | | |
| SYBASE_HOST | |sybase11_db| |
| SYBASE_DATABASE | database name | metbase.db | |
| SYBASE_DATA_PATH | database path | /databases | |
| SYBASE_USER | | mt |
| SYBASE_PASSWORD | | mt |
| **SymmetricDS Paths** | if user has symds settings enabled | | |
| SYM_BIN_PATH | binaries if needed outside app|/data/symmetric_ds/bin | |
| SYM_ENGINE_PATH | | /data/symmetric_ds/engines | |
| SYM_CONF_PATH | | /data/symmetric_ds/conf | |
| SYM_LOG_PATH | | /data/symmetric_ds/logs | |
| **Configure paths** |  |  |  |
| FILE_MANAGER_PATH | calserver filemanager path|/data/calserver/filemanager ||
| DMS_PATH | the dms file folder | /data/calserver/filemanager/dms ||
| INBOX_PATH | path for read inbox files | /mnt/customer-server/filemanager/inbox ||
| REPORTS_PATH | path for report files and report exports | /mnt/customer-server/filemanager/reports ||
| LOCAL_PATH | calserver settings path | /data/calserver/local | |
| **Borg Backup** | | | |
| BORG_PASSPHRASE | | | |
| BORG_REMOTE | Use borgbase | user@reponame.repo.borgbase.com:repo | Set false(empty) to disable remote |
| BORG_REMOTE | Use your-storagebox | | |
| BORG_REMOTE_PASS | | | |
| BORG_REPO_DIR | | | This is repository directory with path (/backups/calserver) and for your-storagebox only |
| STORAGE_BORG_BACKUP | | | |
| **Webdav** | |  | |
| AUTH_TYPE | | Digest | |
| WEBDAV_USERNAME | | alice | |
| WEBDAV_PASSWORD | | secret1234 | |
| WEBDAV_PATH| | | /data/calserver/filemanager | |
| **VIRTUAL HOST** | | | |
| CALSERVER_HOST | hostname for proxy | develop.net-cal.com | |
| ADMINER_HOST | database subdomain for proxy | database.develop.net-cal.com | |
| WEBDAV_HOST | filemanger domain for webdav | filemanager.develop.net-cal.com | |

### Shell commands

shell commands are useful for install purposes or will be needed to control an automaticly action for calServer.

e.g. ./yiic dmsinboxcheck run

==> is looking for new documents in configured folders to insert them in dms and create links for grids

Usage: ./yiic <command-name> [parameters...]

The following commands are available:

| Command | Description | Usage |
| ------ | ------ | ------ |
| backup | **#702** - add tasks for automatic backup of database. Backup database (using borg backup) with condition from backup_action table: current_date >= start AND current_date <= end (type in list [all, database, file]) | ./yiic backup run [--type=all]|
| copyfiletoinbox | **#774** - all x minutes new files should copied from external directory to inbox. Copy files from External Documents Folder to Target Inbox Folder in File Management | ./yiic copyfiletoinbox run |
| copytolocal | Copy files from remote to local when file store is set to one of the types as **FTP**, **SFTP**, **DigitalOcean** | ./yiic copytolocal run |
| deletefiledms | **#671** - add button 'delete all xxx files'. Delete all files from dms that they are searched by parameters | ./yiic deletefiledms run [--name=value1] [--hash=value2] [--major_version=value3] [--minor_version=value4] [--is_latest=value5] [--status=value6] [--filename=value7] [--filetype=value8] [--type=value9] [--link_table=value10] |
| dmsinboxcheck | Move files from inbox to dms | ./yiic dmsinboxcheck run |
| fixdms | **#578** - inbox dms reading makes version when permission is wrong. Check all records in dms from date parameter and delete them if file isn't found then fix version after delete record from dms | ./yiic fixdms run --delete_from_date=value |
| fixfilesetting | **#1120** - old user setting occurring error in grids. Fix file setting contains fields that they don't exist in field configuration | ./yiic fixfilesetting run |
| fixinbox | **#894** - sometimes 1B files after update. Fix file size for all files in inbox which have size is 1 Byte | ./yiic fixinbox run |
| fixversionfordms | **#1133** - fix file management version creation wrong. Fix versions are incorrect | ./yiic fixversionfordms run |
| generatelicense | **#1011** - add licenses.md document in root. Generate licenses.md in protected folder | ./yiic generatelicense run |
| insertcert | **#837** - find all SSL certs on server and then insert to ssl_certificate table | ./yiic insertcert run |
| mailer | Save emails will be sent in queue from mailer actions with condition: current_date >= start AND current_date <= end (limit=0 is all) | ./yiic mailer run [--limit=0] |
| sendemailqueue | Send emails in queue (limit=0 is all) | ./yiic sendemailqueue run [--limit=0] |
| exportreport | Export reports from emails in queue before send them (limit=0 is all) | ./yiic exportreport run [--limit=0] |
| deletereport | Delete reports from emails in queue after sent successfully (limit=0 is all) | ./yiic deletereport run [--limit=0] |
| resetcounter | Reset global counter variables in Rules page | ./yiic resetcounter run |
| transfertomssql | Transfer all inventories from MySQL database to MSSQL database | ./yiic transfertomssql run |
| updatepathforinventory | **#1353** - Update path field on every row in inventory table when run migration is failed | ./yiic updatepathforinventory run |
| fixI4217 | **#1353** - Adjust I4217 field belong to I4201 field that it has value of MTAG field in inventory table | ./yiic fixI4217 run |
| fetchmail | **#1361** - Fetch mails of support module in inbox from user's email and insert to database | ./yiic fetchmail run |
| saveasstatistic | **#1116** - add statistics table for filter values. Save statistics data from advanced filters | ./yiic saveasstatistic run |
| status | **#194** - new status function to set status automatically. Update status for inventory when they satisfy condition in status action | ./yiic status run |
| testcopy | **#578** - Test copy file on local to make sure file has source path is existed | ./yiic testcopy run --source_path=value1 --dest_path=value2 |
| testrestore | Test restore database from sql file and import to database parameter | ./yiic testrestore run --sql_file=value1 --database=value2 |


###  Constants for default value

```sh
[CURRENT_DATE]
[CURRENT_TIME]
[CURRENT_USER]
[CURRENT_DATETIME]
[DUE_DATE]
[LASTMONTH_STARTDAY]
[LASTMONTH_ENDDAY]
[CURRENTMONTH_STARTDAY]
[CURRENTMONTH_ENDDAY]
[CURRENTYEAR_STARTDAY]
[CURRENTYEAR_ENDDAY]
[MONTHBEFORE_LASTMONTH_STARTDAY]
[MONTHBEFORE_LASTMONTH_ENDDAY]
[GLOBAL_COUNTER_1]
[GLOBAL_COUNTER_2]
[YYYY]
[MM]
[DD]
[STATUS_NAME]
[STATUS_COUNT]
[STATUS_SHORT]
[CURRENT_DATE]+[14D]
[CURRENT_DATE]-[14D]
[CURRENT_DATE]+[2M]
[CURRENT_DATE]-[2M]
[CURRENT_DATE]+[1Y]
[CURRENT_DATE]-[1Y]
[CURRENTMONTH_STARTDAY]+[14D]
[CURRENTMONTH_STARTDAY]-[14D]
[CURRENTMONTH_STARTDAY]+[2M]
[CURRENTMONTH_STARTDAY]-[2M]
[CURRENTMONTH_STARTDAY]+[1Y]
[CURRENTMONTH_STARTDAY]-[1Y]
[YYYY]-[MM]-[DD+14]
[DD+14].[MM].[YYYY]
[DD+5].[MM+1].[YYYY]
[DD+5].[MM+2].[YYYY+1]
[I4201][I4202][I4203][I4204][I4205][I4206][I4207][I4208][I4209][I4210][I4211][I4212][I4213][I4214][I4215][I4216][I4217][I4218][I4219][I4220][I4221][I4222][I4223][I4224][I4225][I4226][I4228]
[I4229][I4230][I4231][I4232][I4233][I4234][I4235][I4236][I4237][I4238][I4239][I4240][I4299][I4241][I4242][I4243][I4258][I4244][I4245][I4246][I4247][I4248][I4249][I4250][I4251][I4252][I4253]
[I4254][I4255][I4256][I4257][I4259][I4260][I4261][I4262]
[C2301][C2302][C2303][C2304][C2306][C2307][C2309][C2310][C2311][C2312][C2313][C2314][C2316][C2320][C2321][C2322][C2323][C2324][C2326][C2327][C2328][C2329][C2330][C2331][C2332][C2333][C2334]
[C2335][C2336][C2337][C2338][C2339][C2341][C2362][C2308][C2315][C2342][C2343][C2317][C2318][C2319][C2325][C2350][C2351][C2352][C2353][C2354][C2355][C2356][C2357][C2358][C2359][C2360][C2361]
[C2363][C2381][C2382][C2364][C2365][C2366][C2367][C2368][C2369][C2370][C2371][C2372][C2373][C2374][C2375][C2376][C2377][C2378][C2379][C2380][C2383][C2344][C2345][C2346][C2347][C2384][C2385]
[C2386][C2387][C2388][C2389][C2390][C2391][C2392][C2393][C2394][C2395][C2396]
```

### Change log API

#### Get all versions:
https://domain.com/adminpanel/adminInformation/changeLogApi

#### Get latest version:
https://domain.com/adminpanel/adminInformation/changeLogApi/version/latest

#### Get a version:
https://domain.com/adminpanel/adminInformation/changeLogApi/version/<version>
E.g: https://domain.com/adminpanel/adminInformation/changeLogApi/version/4.0.0

### Development

Want to contribute? Great!


### Todos

 - Write MORE Tests
 - Add Night Mode

License
----
All Rights Reserved by calHelp. Copyright calHelp� 2019. Our application is 100% open-source and ready to customize from back-end to front-end You are fully allowed to extend, customize or redesign only for your licensed business only.
