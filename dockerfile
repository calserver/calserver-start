FROM ubuntu:20.04

ARG DOMAIN

ENV DOMAIN=$DOMAIN
ENV LOG4J_FORMAT_MSG_NO_LOOKUPS=true

RUN set -eux; \
    export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true TZ=Europe/Berlin \
    && export DOCKER_CONTENT_TRUST=1 \
    && export DOCKER_CONTENT_TRUST_SERVER=$DOMAIN \
    && apt-get update \
    # Install tools
    && apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages --no-install-recommends \
        apt-utils tzdata \
        software-properties-common \
        ca-certificates curl gnupg build-essential \
    # Add mssql source
    && echo "ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true" | debconf-set-selections \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/ubuntu/20.04/prod.list > /etc/apt/sources.list.d/mssql-release.list \
    # Add docker source
    && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - \
    && apt-key fingerprint 0EBFCD88 \
    && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
    # Add php source
    && add-apt-repository ppa:ondrej/php -y \
    # Install packages
    && apt-get update --option Acquire::ForceIPv4=true --option Acquire::Retries=100 --option Acquire::http::Timeout=60 \
    && ACCEPT_EULA=Y apt-get install -y --allow-downgrades --allow-remove-essential --allow-change-held-packages --no-install-recommends \
        ttf-mscorefonts-installer \
        fonts-crosextra-carlito msodbcsql17 unixodbc-dev=2.3.7 unixodbc=2.3.7 odbcinst1debian2=2.3.7 odbcinst=2.3.7 mssql-tools libssl1.1 \
        net-tools git zip unzip gettext-base memcached \
        php7.4 php7.4-zip php-pear php7.4-dev php7.4-dom php7.4-fpm nginx php7.4-memcached php7.4-cli php7.4-common php7.4-mbstring php7.4-gd php7.4-mysql php7.4-intl php7.4-xml php7.4-imap php7.4-curl \
        sudo cron mysql-client wget openjdk-8-jre-headless php7.4-ssh2 sendmail apt-transport-https openntpd jq dnsutils libnss-wrapper \
        certbot python3-certbot-nginx letsencrypt gcc g++ openssh-server openssh-client sshpass locales \
        docker-ce-cli \
    # Add php ext
    && pecl install sqlsrv-5.8.1 pdo_sqlsrv-5.8.1 \
    # Install su-exec
    && curl -L https://github.com/ncopa/su-exec/archive/dddd1567b7c76365e1e0aac561287975020a8fad.tar.gz | tar xvz \
    && cd su-exec-* && make && mv su-exec /usr/local/bin && cd .. && rm -rf su-exec-* \
    # Install docker-compose
    && curl -L https://github.com/docker/compose/releases/download/1.21.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose \
    && chmod 755 /usr/local/bin/docker-compose \
    # Install Borg Backup
    && wget https://github.com/borgbackup/borg/releases/download/1.1.13/borg-linux64 \
    && mv borg-linux64 /usr/local/sbin/borg \
    && chown www-data:www-data /usr/local/sbin/borg \
    && chmod 755 /usr/local/sbin/borg \
    # Clean up
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Change the home dir
RUN mkdir -p /app
ENV HOME=/app

# Initial configuration of SQL Server PHP connector
RUN echo "extension=sqlsrv.so" > /etc/php/7.4/fpm/conf.d/sqlsrv.ini && \
    echo "extension=pdo_sqlsrv.so" > /etc/php/7.4/fpm/conf.d/pdo_sqlsrv.ini && \
    echo "extension=sqlsrv.so" > /etc/php/7.4/mods-available/sqlsrv.ini && \
    echo "extension=pdo_sqlsrv.so" > /etc/php/7.4/mods-available/pdo_sqlsrv.ini

# Configure user and permissions
RUN usermod -aG sudo www-data \
    && echo "www-data ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers \
    # Install SQL Server tools
    && echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc \
    && echo "export LC_ALL=de_DE.UTF-8\nexport LANG=de_DE.UTF-8" >> ~/.bashrc \
    && /bin/bash -c "source ~/.bashrc"

# Generate de_DE locale
RUN locale-gen de_DE de_DE.UTF-8 \
    && update-locale LC_ALL=de_DE.UTF-8 LANG=de_DE.UTF-8

# "java.awt.AWTError: Assistive Technology not found: org.GNOME.Accessibility.AtkWrapper"
RUN sed -i 's/^assistive_technologies=/#&/' /etc/java-8-openjdk/accessibility.properties

COPY configs /app/configs

# Configure PHP-FPM to run as the non-root user
RUN mkdir -p /app/php/logs \
    && cp -rf /app/configs/ntpd.conf /etc/ntpd.conf \
    && cp -rf /app/configs/php.ini /etc/php/7.4/fpm/php.ini \
    && cp -rf /app/configs/www-overrides.conf /etc/php/7.4/fpm/pool.d/z-overrides.conf \
    && cp -rf /app/configs/memcached.conf /etc/memcached_z.conf \
    && sed -i "s/pid = \/run\/php\/php7.4-fpm.pid/pid = \/tmp\/php7.4-fpm.pid/" /etc/php/7.4/fpm/php-fpm.conf \
    && sed -i "s/\/var\/log\/php7.4-fpm.log/\/app\/php\/logs\/php7.4-fpm.log/" /etc/php/7.4/fpm/php-fpm.conf \
    && sed -i "s/\/run\/php\/php7.4-fpm.sock/9000/" /etc/php/7.4/fpm/pool.d/www.conf

# Configure Nginx to run as the non-root user
COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf
RUN mkdir -p /app/nginx /app/nginx/sites-enabled /app/nginx/logs \
    && rm -rf /etc/nginx/sites-enabled/default \
    && chmod -R 777 /var/log/nginx

# # Add app source
COPY --chown=www-data:www-data yii /app/yii
COPY --chown=www-data:www-data httpdocs /app/httpdocs

# Install symmetricds
COPY calserver-sync /app/calserver-sync
RUN mkdir -m 777 /app/calserver-sync/logs \
    && mkdir -m 777 /app/calserver-sync/tmp \
    && chmod -R +x /app/calserver-sync/bin/* \
    && chmod -R 777 /app/calserver-sync/engines

# # Add docker script
COPY docker/*.sh /app/docker/

# Add Borg
COPY borg /app/borg
RUN chmod +x /app/borg/*.sh

# Permissions and requirements yii
RUN mkdir -p /app/httpdocs/backup /app/httpdocs/database /app/httpdocs/docker/nginx/ssl \
    && touch /app/httpdocs/.env \
    && chmod +x /app/httpdocs/protected/yiic \
    && chmod -R 0755 /app/httpdocs/protected/extensions/Jasper \
    && chmod -R 0777 /app/httpdocs/.env /app/httpdocs/assets /app/httpdocs/backup /app/httpdocs/database /app/httpdocs/protected/runtime /app/httpdocs/uploads /app/httpdocs/media /app/httpdocs/filemanager /app/httpdocs/docker \
    && chown www-data:www-data /app/httpdocs/.env /app/httpdocs/assets /app/httpdocs/backup /app/httpdocs/database /app/httpdocs/protected/runtime /app/httpdocs/uploads /app/httpdocs/media /app/httpdocs/filemanager /app/httpdocs/docker

RUN mkdir -p /app/log /app/.ssh /app/.config /app/.cache /backup /config /var/local \
    && chmod -R 0777 /app/log /app/.ssh /app/.config /app/.cache /backup /config /var/local \
    && chown www-data:www-data /var/www /var/local

# Copy initdb.d and startdb.d
COPY docker/mysql/initdb.d /docker-entrypoint-initdb.d
COPY docker/mysql/startdb.d /docker-entrypoint-startdb.d
RUN chmod 777 /docker-entrypoint-initdb.d/* /docker-entrypoint-startdb.d/* \
    && chmod +x /docker-entrypoint-initdb.d/*.sh /docker-entrypoint-startdb.d/*.sh 

COPY docker/app/*.sh /app/
RUN chmod +x /app/*.sh /app/docker/*.sh /app/httpdocs/scripts/*.sh

RUN chgrp -R 0 /app \
    && chmod g+rwX -R /app

EXPOSE 8080 8443 80 443

WORKDIR /app

# fix cron
# FIXME: cron not working!
RUN sed -i '/^PATH=/a PIDFILE=/tmp/cron.pid' /etc/crontab \
    && chmod 777 /var/spool/cron/crontabs/ /var/run \
    && chmod gu+s /usr/sbin/cron \
    && chmod gu+rw /var/run

USER 1001
ENTRYPOINT ["/app/run.sh"]
CMD ["/usr/sbin/ntpd", "-v", "-d", "-s"]
