#!/bin/bash
# copy all docker compose files and deploy scripts
cp ./src/*.yml ./src/*.sh ../
# copy local needed configurations for webserver and database
cp -avr ./src/nginx/ ../
cp -avr ./src/mysql/ ../
#copy app template
cp ./src/.env_template ../.env
# make scripts executable
chmod +x ../*.sh
#copy and enable autostart service
cp ./src/calserver-start.service /etc/systemd/system/calserver-start.service
systemctl enable calserver-start.service

