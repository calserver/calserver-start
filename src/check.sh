#!/usr/bin/env bash

source ./.env

RUNNING=`docker inspect "calhelp/calwebapp_bootstrap:${IMAGE_TAG}" | grep Id | sed "s/\"//g" | sed "s/,//g" |  tr -s ' ' | cut -d ' ' -f3`

IMAGE_INFO=`docker image inspect --format '{{json .}}' "calhelp/calwebapp_bootstrap:${IMAGE_TAG}" | jq -r '. | {Id: .Id, Digest: .Digest, RepoDigests: .RepoDigests}'`
LATEST=`echo $IMAGE_INFO | jq -r '.RepoDigests[0]' | sed 's/calhelp\/calwebapp_bootstrap@//g'`

if [ "$RUNNING" == "$LATEST" ]; then
    echo "Image is up to date"
else
    echo "Update"
fi