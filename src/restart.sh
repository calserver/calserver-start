#!/usr/bin/env bash

source ./.env

export SSHPASS=${PASS};
IMAGE="calhelp/calwebapp_bootstrap:${IMAGE_TAG}";

sshpass -e ssh -o StrictHostKeyChecking=no ${USER}@${DOMAIN} IMAGE=${IMAGE} LETSENCRYPT=${LETSENCRYPT} SSL_CERT_PATH=${SSL_CERT_PATH} DOMAIN=${DOMAIN} '
cd /var/calserver;
if [ "${LETSENCRYPT}" = "true" ]; then
    SSL_FILE="${SSL_CERT_PATH}/${DOMAIN}.crt";
    SSL_KEY_FILE="${SSL_CERT_PATH}/${DOMAIN}.key";
    if [[ -f "${SSL_FILE}" && -f "${SSL_KEY_FILE}" ]]; then
        docker-compose -f docker-compose.ssl.yml up -d
    else
        docker-compose -f docker-compose.yml -f docker-compose.https.yml up -d
    fi
else
    docker-compose -f docker-compose.yml up -d;
fi
'