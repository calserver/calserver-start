#!/usr/bin/env bash

source /app/docker/.env

export SSHPASS=${PASS};
IMAGE="calhelp/calwebapp_bootstrap:${IMAGE_TAG}";

sshpass -e ssh -o StrictHostKeyChecking=no ${USER}@${DOMAIN} IMAGE=${IMAGE} '
docker system prune -af;
docker pull "${IMAGE}";