#!/usr/bin/env bash

source ./.env

RUNNING=`docker inspect "calhelp/calwebapp_bootstrap:${IMAGE_TAG}" | grep Id | sed "s/\"//g" | sed "s/,//g" |  tr -s ' ' | cut -d ' ' -f3`
echo "$RUNNING"