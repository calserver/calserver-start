#!/usr/bin/env bash

source ./.env

# Set permission for docker folder
if [[ -n "${DOCKER_PATH}" && -d "${DOCKER_PATH}" ]]; then
    chmod -R 0755 "${DOCKER_PATH}"
    chown -R www-data:www-data "${DOCKER_PATH}"
fi

# Create and set permission for backup folder
if [ -n "${FOLDER_BACKUP}" ]; then
    if [ ! -d "${FOLDER_BACKUP}" ]; then
        mkdir -m 0755 "${FOLDER_BACKUP}"
    else
        chmod -R 0755 "${FOLDER_BACKUP}"
    fi
    chown -R www-data:www-data "${FOLDER_BACKUP}"
fi

# Create and set permission for file manager folder
if [ ! -d "${FILE_MANAGER_PATH}" ]; then
    mkdir -m 0755 "${FILE_MANAGER_PATH}"
else
    chmod -R 0755 "${FILE_MANAGER_PATH}"
fi
chown -R www-data:www-data "${FILE_MANAGER_PATH}"

docker-compose kill
docker rm -f nginx-proxy

if [ "${LETSENCRYPT}" = "true" ]; then
    SSL_FILE="${SSL_CERT_PATH}/${DOMAIN}.crt"
    SSL_KEY_FILE="${SSL_CERT_PATH}/${DOMAIN}.key"
    if [[ -f "${SSL_FILE}" && -f "${SSL_KEY_FILE}" ]]; then
        docker-compose -f docker-compose.ssl.yml up -d
    else
        docker-compose -f docker-compose.yml -f docker-compose.https.yml up -d
    fi
else
    docker-compose -f docker-compose.yml up -d
fi